package main

import (
	"bufio"
	"fmt"
	"os/exec"
	"time"
)

const INTERVAL_PERIOD time.Duration = 24 * time.Hour

const HOUR_TO_TICK int = 02
const MINUTE_TO_TICK int = 00
const SECOND_TO_TICK int = 00

type jobTicker struct {
	timer *time.Timer
}

func main() {
	do_backup()
	jobTicker := &jobTicker{}
	jobTicker.updateTimer()
	for {
		<-jobTicker.timer.C
		fmt.Println(time.Now(), "- just ticked")

		do_backup()

		jobTicker.updateTimer()
	}
}

func do_backup() {
	fmt.Println("Starting Backup")

	fmt.Println("Commancing: ", "rsync", "-ar", "--partial", "--delete", "--exclude=\"/sync/source/lost+found\"", "--exclude=\"/sync/source/.deleted\"", "/sync/source/", "/sync/dest/backup_simone_hdd")

	cmd := exec.Command("rsync", "-ar", "--partial", "--delete", "--exclude=\"/sync/source/lost+found\"", "--exclude=\"/sync/source/.deleted\"", "/sync/source/", "/sync/dest/backup_simone_hdd")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}

	err = cmd.Start()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}

	scanner := bufio.NewScanner(stdout)
	scanner.Split(bufio.ScanLines)
	fmt.Println("Output:")
	for scanner.Scan() {
		m := scanner.Text()
		fmt.Println(m)
	}
}

func (t *jobTicker) updateTimer() {
	location, err := time.LoadLocation("Europe/Berlin")
	if err != nil {
		location = time.Local
	}
	nextTick := time.Date(time.Now().Year(), time.Now().Month(),
		time.Now().Day(), HOUR_TO_TICK, MINUTE_TO_TICK, SECOND_TO_TICK, 0, location)
	if !nextTick.After(time.Now()) {
		nextTick = nextTick.Add(INTERVAL_PERIOD)
	}
	fmt.Println(nextTick, "- next tick")
	diff := nextTick.Sub(time.Now())
	if t.timer == nil {
		t.timer = time.NewTimer(diff)
	} else {
		t.timer.Reset(diff)
	}
}
